package com.example.tuparquekotlin.ui.modelo

data class Parques(val nombreParque: String = "", val tieneGimnasio: String = "", val canchas: String = "")
//data class me ofrece el toString para mostrar los datos
